package view;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Rectangle;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML
    Rectangle rect0;
    @FXML
    Rectangle rect1;
    @FXML
    Rectangle rect2;
    @FXML
    Rectangle rect3;
    @FXML
    Rectangle rect4;
    @FXML
    Rectangle rect5;
    @FXML
    Rectangle rect6;
    @FXML
    Rectangle rect7;
    @FXML
    Rectangle rect8;
    @FXML
    Rectangle rect9;
    @FXML
    Rectangle rect10;
    @FXML
    Rectangle rect11;
    @FXML
    Rectangle rect12;
    @FXML
    Rectangle rect13;
    @FXML
    Rectangle rect14;
    @FXML
    Rectangle rect15;
    @FXML
    Rectangle rect16;
    @FXML
    Rectangle rect17;
    @FXML
    Rectangle rect18;
    @FXML
    Rectangle rect19;
    @FXML
    Rectangle rect20;
    @FXML
    Rectangle rect21;
    @FXML
    Rectangle rect22;
    @FXML
    Rectangle rect23;
    @FXML
    Rectangle rect24;
    @FXML
    Rectangle rect25;
    @FXML
    Rectangle rect26;
    @FXML
    Rectangle rect27;
    @FXML
    Rectangle rect28;
    @FXML
    Rectangle rect29;
    @FXML
    Rectangle rect30;
    @FXML
    Rectangle rect31;
    @FXML
    Rectangle rect32;
    @FXML
    Rectangle rect33;
    @FXML
    Rectangle rect34;
    @FXML
    Rectangle rect35;
    @FXML
    Rectangle rect36;
    @FXML
    Rectangle rect37;
    @FXML
    Rectangle rect38;
    @FXML
    Rectangle rect39;
    @FXML
    Rectangle rect40;
    @FXML
    Rectangle rect41;
    @FXML
    Rectangle rect42;
    @FXML
    Rectangle rect43;
    @FXML
    Rectangle rect44;
    @FXML
    Rectangle rect45;
    @FXML
    Rectangle rect46;
    @FXML
    Rectangle rect47;
    @FXML
    Rectangle rect48;
    @FXML
    Rectangle rect49;
    @FXML
    Rectangle rect50;
    @FXML
    Rectangle rect51;
    @FXML
    Rectangle rect52;
    @FXML
    Rectangle rect53;
    @FXML
    Rectangle rect54;
    @FXML
    Rectangle rect55;
    @FXML
    Rectangle rect56;
    @FXML
    Rectangle rect57;
    @FXML
    Rectangle rect58;
    @FXML
    Rectangle rect59;
    @FXML
    Rectangle rect60;
    @FXML
    Rectangle rect61;
    @FXML
    Rectangle rect62;
    @FXML
    Rectangle rect63;

    int x;
    int y;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        rect0.setOnMouseClicked(rectangleEventHandler);
        rect1.setOnMouseClicked(rectangleEventHandler);
        rect2.setOnMouseClicked(rectangleEventHandler);
        rect3.setOnMouseClicked(rectangleEventHandler);
        rect4.setOnMouseClicked(rectangleEventHandler);
        rect5.setOnMouseClicked(rectangleEventHandler);
        rect6.setOnMouseClicked(rectangleEventHandler);
        rect7.setOnMouseClicked(rectangleEventHandler);
        rect8.setOnMouseClicked(rectangleEventHandler);
        rect9.setOnMouseClicked(rectangleEventHandler);
        rect10.setOnMouseClicked(rectangleEventHandler);
        rect11.setOnMouseClicked(rectangleEventHandler);
        rect12.setOnMouseClicked(rectangleEventHandler);
        rect13.setOnMouseClicked(rectangleEventHandler);
        rect14.setOnMouseClicked(rectangleEventHandler);
        rect15.setOnMouseClicked(rectangleEventHandler);
        rect16.setOnMouseClicked(rectangleEventHandler);
        rect17.setOnMouseClicked(rectangleEventHandler);
        rect18.setOnMouseClicked(rectangleEventHandler);
        rect19.setOnMouseClicked(rectangleEventHandler);
        rect20.setOnMouseClicked(rectangleEventHandler);
        rect21.setOnMouseClicked(rectangleEventHandler);
        rect22.setOnMouseClicked(rectangleEventHandler);
        rect23.setOnMouseClicked(rectangleEventHandler);
        rect24.setOnMouseClicked(rectangleEventHandler);
        rect25.setOnMouseClicked(rectangleEventHandler);
        rect26.setOnMouseClicked(rectangleEventHandler);
        rect27.setOnMouseClicked(rectangleEventHandler);
        rect28.setOnMouseClicked(rectangleEventHandler);
        rect29.setOnMouseClicked(rectangleEventHandler);
        rect30.setOnMouseClicked(rectangleEventHandler);
        rect31.setOnMouseClicked(rectangleEventHandler);
        rect32.setOnMouseClicked(rectangleEventHandler);
        rect33.setOnMouseClicked(rectangleEventHandler);
        rect34.setOnMouseClicked(rectangleEventHandler);
        rect35.setOnMouseClicked(rectangleEventHandler);
        rect36.setOnMouseClicked(rectangleEventHandler);
        rect37.setOnMouseClicked(rectangleEventHandler);
        rect38.setOnMouseClicked(rectangleEventHandler);
        rect39.setOnMouseClicked(rectangleEventHandler);
        rect40.setOnMouseClicked(rectangleEventHandler);
        rect41.setOnMouseClicked(rectangleEventHandler);
        rect42.setOnMouseClicked(rectangleEventHandler);
        rect43.setOnMouseClicked(rectangleEventHandler);
        rect44.setOnMouseClicked(rectangleEventHandler);
        rect45.setOnMouseClicked(rectangleEventHandler);
        rect46.setOnMouseClicked(rectangleEventHandler);
        rect47.setOnMouseClicked(rectangleEventHandler);
        rect48.setOnMouseClicked(rectangleEventHandler);
        rect49.setOnMouseClicked(rectangleEventHandler);
        rect50.setOnMouseClicked(rectangleEventHandler);
        rect51.setOnMouseClicked(rectangleEventHandler);
        rect52.setOnMouseClicked(rectangleEventHandler);
        rect53.setOnMouseClicked(rectangleEventHandler);
        rect54.setOnMouseClicked(rectangleEventHandler);
        rect55.setOnMouseClicked(rectangleEventHandler);
        rect56.setOnMouseClicked(rectangleEventHandler);
        rect57.setOnMouseClicked(rectangleEventHandler);
        rect58.setOnMouseClicked(rectangleEventHandler);
        rect59.setOnMouseClicked(rectangleEventHandler);
        rect60.setOnMouseClicked(rectangleEventHandler);
        rect61.setOnMouseClicked(rectangleEventHandler);
        rect62.setOnMouseClicked(rectangleEventHandler);
        rect63.setOnMouseClicked(rectangleEventHandler);



    }

    private EventHandler<? super MouseEvent> rectangleEventHandler = event -> {
        String id = ((Rectangle) event.getSource()).getId();
        String[] idChars = id.split("");
        StringBuilder idNum = new StringBuilder();
        for (int i = 4; i < idChars.length; i++) {
            idNum.append(idChars[i]);
        }

        int squareId = Integer.parseInt(idNum.toString());
        x = squareId / 8;
        y = squareId % 8;
    };
}
