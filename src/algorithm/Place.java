package algorithm;

import java.util.ArrayList;

public class Place {
    public int x;
    public int y;
    public int placeNum;
    public boolean checked = false;

    public ArrayList<Place> neighbors;

    public Place(int x, int y){
        this.x = x;
        this.y = y;
        this.placeNum = (x * 8) + y;
        neighbors = new ArrayList<>();
    }

    public void addNeighbor(Place p){
        neighbors.add(p);
    }
}