package algorithm;

import java.util.ArrayList;

public class Algorithm {
    public static void main(String[] args) {


        //generate adding rectangle action listener
//        for (int i = 0; i < 64; i++) {
//            System.out.println("rect"+i+".setOnMouseClicked(rectangleEventHandler);");
//        }
        //generate adding to controller
//        for (int i = 0; i < 64; i++) {
//            System.out.println("@FXML\n" +
//                    "Rectangle rect"+i+";");
//        }

        //generate rectangles codes
//        boolean white= false;
//        for (int i = 0; i < 8; i++) {
//            if (i%2 == 0){
//                white = true;
//            } else
//                white = false;
//            for (int j = 0; j < 8; j++) {
//
//                if (white) {
//                    System.out.println("<Rectangle fx:id=\"rect"+((i*8) + j)+"\" arcHeight=\"5.0\" arcWidth=\"5.0\" fill=\"#94979a\" height=\"50.0\" layoutY=\""+i*50+"\" layoutX=\""+j*50+"\" stroke=\"BLACK\" strokeType=\"INSIDE\" width=\"50.0\" />\n");
//                    white = false;
//                }6Btb


//                else {
//                    System.out.println("<Rectangle fx:id=\"rect"+((i*8) + j)+"\" arcHeight=\"5.0\" arcWidth=\"5.0\" fill=\"#ffffff\" height=\"50.0\" layoutY=\""+i*50+"\" layoutX=\""+j*50+"\" stroke=\"BLACK\" strokeType=\"INSIDE\" width=\"50.0\" />\n");
//                    white = true;
//                }
//            }
//        }


        //test run


        long sTime = System.currentTimeMillis();
        new Algorithm().start(0, 0);
        long eTime = System.currentTimeMillis();

        System.out.println("\n\ntime:  "+ (eTime - sTime) + "   milli seconds");
    }

    private ArrayList<Place> places = new ArrayList<>();
    private void start(int x, int y){
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                places.add(new Place(i, j));
            }
        }


        for (Place place : places) {
            for (Place possibleN : places) {
                if (place.x == possibleN.x-2 && place.y ==  possibleN.y-1){
                    place.addNeighbor(possibleN);
                } else if (place.x == possibleN.x-2 && place.y ==  possibleN.y+1){
                    place.addNeighbor(possibleN);
                } else if (place.x == possibleN.x-1 && place.y ==  possibleN.y+2){
                    place.addNeighbor(possibleN);
                } else if (place.x == possibleN.x+1 && place.y ==  possibleN.y+2){
                    place.addNeighbor(possibleN);
                } else if (place.x == possibleN.x+2 && place.y ==  possibleN.y+1){
                    place.addNeighbor(possibleN);
                } else if (place.x == possibleN.x+2 && place.y ==  possibleN.y-1){
                    place.addNeighbor(possibleN);
                } else if (place.x == possibleN.x+1 && place.y ==  possibleN.y-2){
                    place.addNeighbor(possibleN);
                } else if (place.x == possibleN.x-1 && place.y ==  possibleN.y-2){
                    place.addNeighbor(possibleN);
                }
            }
        }
        Place place = new Place(-1, 1);
        for (Place p :
                places) {
            if (p.x == x && p.y == y){
                place = p;
                break;
            }
        }
        algorithm(place);
    }

    ArrayList<Place> rout = new ArrayList<>();
    private boolean algorithm(Place place){
        rout.add(place);
        place.checked = true;
        if (rout.size()==64){
            System.out.println("results are : ");
            printResult();
            return true;
        }
        for (int i = 0; i < place.neighbors.size(); i++) {
            if (!place.neighbors.get(i).checked){
                boolean con = algorithm(place.neighbors.get(i));
                if (con)
                    return true;
            }
            if (i == place.neighbors.size()-1){
                rout.get(rout.size()-1).checked = false;
                rout.remove(rout.size()-1);
            }
        }

        return false;
    }

    private void printResult(){
        for (Place p : rout) {
            System.out.print(((p.x * 8) + p.y) + "      ");
        }
    }





















}
