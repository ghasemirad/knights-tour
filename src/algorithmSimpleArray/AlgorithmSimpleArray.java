package algorithmSimpleArray;

public class AlgorithmSimpleArray {

    public static void main(String[] args) {

        long sTime = System.currentTimeMillis();
        //enter the number of row and column here!
        new AlgorithmSimpleArray().start(0, 0);
        long eTime = System.currentTimeMillis();

        System.out.println("\n\ntime:  "+ (eTime - sTime) + "   milli seconds");
    }

    private PlaceSimpleArray[] places = new PlaceSimpleArray[64];
    private void start(int x, int y){
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                places[i*8 + j] = new PlaceSimpleArray(i, j);
            }
        }

        for (int i = 0; i < 64; i++) {
            for (int j = 0; j < 64; j++) {
                if (places[i].x == places[j].x-2 && places[i].y ==  places[j].y-1){
                    places[i].addNeighbor(places[j]);
                } else if (places[i].x == places[j].x-2 && places[i].y ==  places[j].y+1){
                    places[i].addNeighbor(places[j]);
                } else if (places[i].x == places[j].x-1 && places[i].y ==  places[j].y+2){
                    places[i].addNeighbor(places[j]);
                } else if (places[i].x == places[j].x+1 && places[i].y ==  places[j].y+2){
                    places[i].addNeighbor(places[j]);
                } else if (places[i].x == places[j].x+2 && places[i].y ==  places[j].y+1){
                    places[i].addNeighbor(places[j]);
                } else if (places[i].x == places[j].x+2 && places[i].y ==  places[j].y-1){
                    places[i].addNeighbor(places[j]);
                } else if (places[i].x == places[j].x+1 && places[i].y ==  places[j].y-2){
                    places[i].addNeighbor(places[j]);
                } else if (places[i].x == places[j].x-1 && places[i].y ==  places[j].y-2){
                    places[i].addNeighbor(places[j]);
                }
            }
        }
        algorithme(places[x*8 + y]);

        setAllRoutesToNull();
    }



    private PlaceSimpleArray[] rout = new PlaceSimpleArray[64];
    private void setAllRoutesToNull(){
        for (int i = 0; i < 64; i++) {
            rout[i] = null;
        }
    }

    private int getLastNullFromArray(){
        for (int i = 0; i < 64; i++) {
            if (rout[i] == null) {
                return i;
            }
        }

        return 65;
    }

    private boolean algorithme(PlaceSimpleArray place){
        rout[getLastNullFromArray()] = place;
        place.checked = true;
        if (getLastNullFromArray() == 65){
            System.out.println("results are : ");
            printResult();
            return true;
        }
        for (int i = 0; i < place.neighbors.length; i++) {
            if (place.neighbors[i] != null && !place.neighbors[i].checked){
                boolean con = algorithme(place.neighbors[i]);
                if (con)
                    return true;
            }
            if (i == place.neighbors.length - 1){
                rout[getLastNullFromArray()-1].checked = false;
                rout[getLastNullFromArray()-1] = null;
            }
        }
        return false;
    }
    private void printResult(){
        for (PlaceSimpleArray placeSimpleArray : rout) {
            System.out.print(placeSimpleArray.placeNum + "\t\t");
        }
    }
}
