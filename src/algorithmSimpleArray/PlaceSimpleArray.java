package algorithmSimpleArray;

public class PlaceSimpleArray {
    public int x;
    public int y;
    public int placeNum;
    public boolean checked = false;

    public PlaceSimpleArray[] neighbors;

    public PlaceSimpleArray(int x, int y){
        this.x = x;
        this.y = y;
        this.placeNum = (x * 8) + y;
        neighbors = new PlaceSimpleArray[8];
        setAllNesToNull();
    }

    private void setAllNesToNull(){
        for (int i = 0; i < 8; i++) {
            neighbors[i] = null;
        }
    }
    public void addNeighbor(PlaceSimpleArray p){
        neighbors[getLlastNotNullIndex()] = p;
    }

    private int getLlastNotNullIndex(){
        for (int i = 0; i < 8; i++) {
            if (neighbors[i] == null)
                return i;
        }
        return -1;
    }

}
